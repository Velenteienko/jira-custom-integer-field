package ut.com.simple.jira.customfields;

import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.simple.jira.customfields.impl.IntegerCustomFieldImpl;
import org.junit.Test;
import junit.framework.Assert;

/**
 * @since 3.5
 */
public class IntegerCustomFieldComponentImplUnitTest {

    @Test
    public void getIntegerValue_ValueIsNull() throws FieldValidationException {
        final Integer intValue = null;
        IntegerCustomFieldImpl integerCustomFieldComponent = new IntegerCustomFieldImpl(null, null);
        Assert.assertEquals(intValue, integerCustomFieldComponent.getSingularObjectFromString(null));
    }

    @Test
    public void getIntegerValue_OK() throws FieldValidationException {
        final Integer intValue = 12345;
        IntegerCustomFieldImpl integerCustomFieldComponent = new IntegerCustomFieldImpl(null, null);
        Assert.assertEquals(intValue, integerCustomFieldComponent.getSingularObjectFromString("12345"));
    }

    @Test(expected=FieldValidationException.class)
    public void getIntegerValue_NOK() throws FieldValidationException {
        final Integer intValue = 12345;
        IntegerCustomFieldImpl integerCustomFieldComponent = new IntegerCustomFieldImpl(null, null);
        Assert.assertEquals(intValue, integerCustomFieldComponent.getSingularObjectFromString("12345-a"));
    }

    @Test
    public void getIntegerValue_NotInteger_checkException() throws FieldValidationException {
        IntegerCustomFieldImpl integerCustomFieldComponent = new IntegerCustomFieldImpl(null, null);
        try {
            integerCustomFieldComponent.getSingularObjectFromString("12345-a");
            Assert.fail("Test of validation should filed.");
        } catch (FieldValidationException ex) {
            Assert.assertEquals("Input value not a integer or larger than integer!", ex.getMessage());
        }
    }

    @Test
    public void getIntegerValue_AnotherType_checkException() throws FieldValidationException {
        IntegerCustomFieldImpl integerCustomFieldComponent = new IntegerCustomFieldImpl(null, null);
        try {
            integerCustomFieldComponent.getSingularObjectFromString("12.345");
            Assert.fail("Test of validation should filed.");
        } catch (FieldValidationException ex) {
            Assert.assertEquals("Input value not a integer or larger than integer!", ex.getMessage());
        }
    }

    @Test
    public void getIntegerValue_NegativeIntegerValue_checkException() throws FieldValidationException {
        IntegerCustomFieldImpl integerCustomFieldComponent = new IntegerCustomFieldImpl(null, null);
        try {
            integerCustomFieldComponent.getSingularObjectFromString("-12345");
            Assert.fail("Test of validation should filed.");
        } catch (FieldValidationException ex) {
            Assert.assertEquals("Input value must be positive only!", ex.getMessage());
        }
    }
}
