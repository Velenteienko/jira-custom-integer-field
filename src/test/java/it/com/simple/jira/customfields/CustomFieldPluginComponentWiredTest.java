package it.com.simple.jira.customfields;

import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.sal.api.ApplicationProperties;
import com.simple.jira.customfields.api.CustomFieldPluginComponent;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by admin on 1/10/2017.
 */


@RunWith(AtlassianPluginsTestRunner.class)
public class CustomFieldPluginComponentWiredTest
{

    private final ApplicationProperties applicationProperties;
    private final CustomFieldPluginComponent customFieldPluginComponent;

    public CustomFieldPluginComponentWiredTest(ApplicationProperties applicationProperties,
                                                CustomFieldPluginComponent customFieldPluginComponent)
    {
        this.applicationProperties = applicationProperties;
        this.customFieldPluginComponent = customFieldPluginComponent;
    }


    @Test
    public void testCheckComponentNameAndDisplayName() {

        final String componentName = "customFieldIntegerTypePluginComponent:";
        Assert.assertEquals("Display name is match to component name!",componentName+applicationProperties.getDisplayName() ,customFieldPluginComponent.getName());
    }

}
