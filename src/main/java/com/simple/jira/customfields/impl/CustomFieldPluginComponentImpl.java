package com.simple.jira.customfields.impl;

import com.atlassian.sal.api.ApplicationProperties;
import com.simple.jira.customfields.api.CustomFieldPluginComponent;


public class CustomFieldPluginComponentImpl implements CustomFieldPluginComponent
{
    private ApplicationProperties applicationProperties;

    public CustomFieldPluginComponentImpl(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
    }

    public String getName()
    {
        if(applicationProperties != null)
        {
            if (!applicationProperties.getDisplayName().isEmpty()) {
                return "customFieldIntegerTypePluginComponent:" + applicationProperties.getDisplayName();
            }
        }
        
        return "customFieldIntegerTypePluginComponent";
    }




}