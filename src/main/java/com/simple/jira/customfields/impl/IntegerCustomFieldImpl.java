package com.simple.jira.customfields.impl;

import com.atlassian.jira.issue.customfields.impl.AbstractSingleFieldType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class IntegerCustomFieldImpl extends AbstractSingleFieldType<Integer> {

    private static final Logger logger = LoggerFactory.getLogger(IntegerCustomFieldImpl.class);

    public IntegerCustomFieldImpl(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager) {
        super(customFieldValuePersister, genericConfigManager);
    }

    @Override
    protected PersistenceFieldType getDatabaseType() {
        logger.info("Persistence field type: " + PersistenceFieldType.TYPE_LIMITED_TEXT);
        return PersistenceFieldType.TYPE_LIMITED_TEXT;
    }

    @Override
    protected Object getDbValueFromObject(final Integer customfieldObj) {
        return getStringFromSingularObject(customfieldObj);
    }

    @Override
    protected Integer getObjectFromDbValue(final Object dbValue) throws FieldValidationException {
        return getSingularObjectFromString((String) dbValue);
    }

    public String getStringFromSingularObject(final Integer singularObj) {
        if (singularObj == null) {
            logger.warn("Singular object is null");
            return null;
        }
        logger.info("Singular object: "+singularObj.toString());
        return singularObj.toString();
    }

    public Integer getSingularObjectFromString(final String inputString) throws FieldValidationException {
        if (inputString == null) {
            logger.warn("Input value is null");
            return null;
        }
        Integer integerValue;
        try {
            integerValue = Integer.parseInt(inputString);
            if (integerValue < 0) {
                logger.warn("Input value must be larger that 0");
                throw new FieldValidationException("Input value must be positive only!");
            }
        }
        catch (NumberFormatException ex) {
            logger.error("Input value not a integer type");
            throw new FieldValidationException("Input value not a integer or larger than integer!");
        }
        logger.info("Returned value is: "+ integerValue);
        return integerValue;
    }
}