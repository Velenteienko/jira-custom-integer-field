package com.simple.jira.customfields.api;

public interface CustomFieldPluginComponent
{
    String getName();
}